# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Nikita Travkin <nikitos.tr@gmail.com>
# Co-Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=device-wileyfox-crackling
pkgdesc="Wileyfox Swift"
pkgver=3
pkgrel=3
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base mkbootimg soc-qcom-msm8916"
makedepends="devicepkg-dev"
source="deviceinfo"
subpackages="
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-kernel-mainline-modem:kernel_mainline_modem
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-nonfree-firmware-modem:nonfree_firmware_modem
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel (no modem, audio routed directly)"
	depends="linux-postmarketos-qcom-msm8916"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline_modem() {
	pkgdesc="Close to mainline kernel (non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="GPU/WiFi/BT/Video(/Modem) firmware"
	depends="linux-firmware-qcom firmware-wileyfox-crackling-venus firmware-wileyfox-crackling-wcnss"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem() {
	pkgdesc="Modem firmware"
	depends="firmware-wileyfox-crackling-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem"
	mkdir "$subpkgdir"
}

sha512sums="d1d9a7be40ff1273c844544f80788270e92c409add355c30500f4f6405f688744bd944c192330e4c62e256bd53f26772af4ec59a62861ac71110d993c83d6c14  deviceinfo"
